import {Component, OnInit, ElementRef} from 'angular2/core';

declare var $: any;

@Component({
    selector: 'sideBar',
    templateUrl: 'app/ts/shared/sidebar/sidebar.html'
})

export class SidebarComponent implements OnInit{

	constructor(private _elRef : ElementRef){

	}

	ngOnInit(){
		console.log($('.xyz'));
		$('.xyz').on('click', function() {
			alert("dada");
		});
		// $(this._elRef.nativeElement).children(':first')['.button-collapse'](this._config); 
		$('.button-collapse').sideNav({
				menuWidth: 300, // Default is 240
				edge: 'right', // Choose the horizontal origin
				closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
			}
		);

		$('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrain_width: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left' // Displays dropdown with edge aligned to the left of button
    }
		);
	}
}
