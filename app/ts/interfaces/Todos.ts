export class Todo{
	public name: string;
	public status: boolean;
	public _id: string;

	constructor(name : string, status : boolean, _id : string){
		this.name = name;
		this.status = status || false;
		this._id = _id;
	}
}