import {Component, OnInit} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';


declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'hm-email-delegation',
	templateUrl: 'app/ts/components/emailDelegation/emailDelegation.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: []
})

export class EmailDelegationComponent implements OnInit {
	title: string = "Email Delegation";
	constructor(private router: Router) { };

	ngOnInit() {

		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

		Materialize.updateTextFields();
	}
}
