import {Component, OnInit} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';

import {AuthenticationService} from '../../services/authentication.service';

declare var Materialize: any;

@Component({
    selector : 'hm-login',
    templateUrl : 'app/ts/components/loginForm/loginform.component.html',
    directives : [ROUTER_DIRECTIVES],
    providers : [AuthenticationService]
})

export class LoginComponent implements OnInit {
	
	login: string = "Login";
	signup: string = "Sign up";
	ngOnInit(){
		Materialize.updateTextFields();
	}


	public details = { username: '', password: '' };

	constructor(private router : Router, public auth:AuthenticationService) { };

	onSubmit(){
		// console.log("dada");
		if( this.details.username == 'test' && this.details.password == 
			'test'){
			this.router.navigate(['Dashboard']);
			
		}
		// this.auth.login(this.details.username, this.details.password, function(status){
		// 	if (status === "Authentication Success") {
		// 		console.log(status);
		// 		console.log(this.parent.details);
		// 		// this.location.replaceState('/')
		// 	}
		// });

	}
}
