import {Component, OnInit} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';


declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'hm-private-messages',
	templateUrl: 'app/ts/components/privateMessages/privateMessages.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: []
})

export class PrivateMessagesComponent implements OnInit {
	title: string = "Email Delegation : Which Messages are private?";
	constructor(private router: Router) { };

	ngOnInit() {

	}
}
