import {Component, OnInit} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';

import {AuthenticationService} from '../../services/authentication.service';

declare var Materialize: any;

@Component({
	selector: 'hm-signup',
	templateUrl: 'app/ts/components/signup/signup.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [AuthenticationService]
})

export class SignupComponent implements OnInit {
	title: string = "Sign up";

	ngOnInit() {
		Materialize.updateTextFields();
	}


	public details = { username: '', password: '' };

	constructor(private router: Router) { };

	onSubmit() {
		// console.log("dada");
		if (this.details.username == 'test' && this.details.password ==
			'test') {
			this.router.navigate(['Login']);

		}
		// this.auth.login(this.details.username, this.details.password, function(status){
		// 	if (status === "Authentication Success") {
		// 		console.log(status);
		// 		console.log(this.parent.details);
		// 		// this.location.replaceState('/')
		// 	}
		// });

	}
}
