import {Component, OnInit} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';


declare var $: any;

@Component({
	selector: 'hm-dashboard',
	templateUrl: 'app/ts/components/dashboard/dashboard.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: []
})

export class DashboardComponent implements OnInit{
	title: string = "Dashboard";
	constructor(private router: Router) { };

	ngOnInit() {

		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
	}
}
