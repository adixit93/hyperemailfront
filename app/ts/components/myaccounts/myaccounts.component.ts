import {Component, OnInit} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';


declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'hm-myaccounts',
	templateUrl: 'app/ts/components/myaccounts/myaccounts.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: []
})

export class MyAccountsComponent implements OnInit {
	title: string = "Connect to my Accounts";
	constructor(private router: Router) { };

	ngOnInit() {
		Materialize.updateTextFields();
		  $('.modal-trigger').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      // ready: function() { alert('Ready'); }, // Callback for Modal open
      // complete: function() { alert('Closed'); } // Callback for Modal close
    });

	}
}
