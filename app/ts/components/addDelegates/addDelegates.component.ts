import {Component, OnInit} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';


declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'hm-add-delegates',
	templateUrl: 'app/ts/components/addDelegates/addDelegates.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: []
})

export class AddDelegatesComponent implements OnInit {
	title: string = "Email Delegation : Who may help me?";
	constructor(private router: Router) { };

	ngOnInit() {

		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

		$('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrain_width: true, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left' // Displays dropdown with edge aligned to the left of button
    });

		Materialize.updateTextFields();
	}
}
