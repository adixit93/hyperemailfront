import {Component, OnInit} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';


declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'hm-private-messages',
	templateUrl: 'app/ts/components/assignPermission/assignPermission.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: []
})

export class AssignPermissionComponent implements OnInit {
	title: string = "Email Delegation : What can they do on my behalf?";
	constructor(private router: Router) { };

	ngOnInit() {

	}
}
