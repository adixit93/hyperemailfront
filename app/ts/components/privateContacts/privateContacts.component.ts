import {Component, OnInit, AfterContentInit, AfterViewChecked, AfterContentChecked} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';


declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'hm-private-contacts',
	templateUrl: 'app/ts/components/privateContacts/privateContacts.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: []
})

export class PrivateContactsComponent implements OnInit {
	title: string = "Email Delegation : Which Contacts are private?";
	constructor(private router: Router) { };

	ngOnInit() {

		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

		Materialize.updateTextFields();

		setTimeout(function() {
			$('select').material_select();
		});

		$('.modal-trigger').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      // ready: function() { alert('Ready'); }, // Callback for Modal open
      // complete: function() { alert('Closed'); } // Callback for Modal close
    });

	}
}
