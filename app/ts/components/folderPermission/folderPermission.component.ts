import {Component, OnInit, AfterContentInit, AfterViewChecked, AfterContentChecked} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';


declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'hm-folder-permissions',
	templateUrl: 'app/ts/components/folderPermission/folderPermission.component.html',
	directives: [ROUTER_DIRECTIVES],	
	providers: []
})

export class FolderPermissionComponent implements OnInit {
	title: string = "Email Delegation : Which Folders may they access?";
	constructor(private router: Router) { };

	ngOnInit() {

		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

		Materialize.updateTextFields();

		setTimeout(function() {
			$('select').material_select();
		});

		$('.modal-trigger').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      // ready: function() { alert('Ready'); }, // Callback for Modal open
      // complete: function() { alert('Closed'); } // Callback for Modal close
    });

    $('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
			constrain_width: true, // Does not change width of dropdown to that of the activator
			hover: true, // Activate on hover
			gutter: 0, // Spacing from edge
			belowOrigin: true, // Displays dropdown below the button
			alignment: 'right' // Displays dropdown with edge aligned to the left of button
		});

	}


	folders = ['Inbox', 'Sent', 'Personal', 'Updates', 'Social','All Others'];
	folderss = ['Inbox', 'Office'];
}
