import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES, RouteParams, RouteConfig, Router} from 'angular2/router';

import {LoginComponent} from './components/loginForm/loginform.component';
import {SignupComponent} from './components/signup/signup.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {MyAccountsComponent} from './components/myaccounts/myaccounts.component';
import {EmailDelegationComponent} from './components/emailDelegation/emailDelegation.component';
import {DelegationComponent} from './components/delegation/delegation.component';
import {AddDelegatesComponent} from './components/addDelegates/addDelegates.component';
import {FolderPermissionComponent} from './components/folderPermission/folderPermission.component';
import {PrivateMessagesComponent} from './components/privateMessages/privateMessages.component';
import {PrivateContactsComponent} from './components/privateContacts/privateContacts.component';
import {AssignPermissionComponent} from './components/assignPermission/assignPermission.component';

import {NavbarComponent} from './shared/navbar/navbar.component';
import {FooterComponent} from './shared/footer/footer.component';

@Component({
    selector: 'hyper-email',
    directives: [ROUTER_DIRECTIVES, NavbarComponent, FooterComponent],
    templateUrl: 'app/ts/app.component.html'
})

@RouteConfig([
	{path: '/', redirectTo: ['Login'] },
	{ path: '/login', name: 'Login', component: LoginComponent },
	{ path: '/signup', name: 'Signup', component: SignupComponent },
	{ path: '/dashboard', name: 'Dashboard', component: DashboardComponent },
	{ path: '/accounts', name: 'Accounts', component: MyAccountsComponent },
	{ path: '/delegation', name: 'Delegation', component: EmailDelegationComponent },
	{ path: '/add/delegates', name: 'AddDelegates', component: AddDelegatesComponent },
	{ path: '/folderAccess', name: 'FolderAccess', component: FolderPermissionComponent },
	{ path: '/private/contacts', name: 'PrivateContacts', component: PrivateContactsComponent },
	{ path: '/private/messages', name: 'PrivateMessages', component: PrivateMessagesComponent },
	{ path: '/permissions', name: 'Permissions', component: AssignPermissionComponent },
	{ path: '/dashboard/emaildelegations', name: 'EmailDelegation', component: DelegationComponent },
])

export class AppComponent {
}
