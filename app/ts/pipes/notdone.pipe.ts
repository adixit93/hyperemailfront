import {Pipe} from "angular2/core";

@Pipe({
	name: 'notdone'
})

export class NotDonePipe {
	transform(arr) {
		var d = [];
		for (var i = 0; i < arr.length; i++) {
			if (arr[i].status == false)
				d.push(arr[i]);
		}
		return d;
	}
}