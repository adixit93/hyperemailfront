import {Pipe, PipeTransform} from "angular2/core";

@Pipe({
	name: 'done'
})

export class DonePipe implements PipeTransform{
	transform(arr){
		// return arr.filter((item)=> item.status == "true");
		var d = [];
		for (var i = 0; i < arr.length; i++) {
			if (arr[i].status == true)
				d.push(arr[i]);
		}
		return d;
	}
}