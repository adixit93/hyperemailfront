import {Injectable} from "angular2/core";

@Injectable()
export class AuthenticationService{
	token: string;

	constructor(){
		this.token = localStorage.getItem('token');
	}

	login(username : string, password : string , callback){

		this.token = "login"
		if (username == 'test' && password == 'test') {
			localStorage.setItem('token', this.token)
			callback("Authentication Success");
		}
		else callback("Authentication Failure");
	}

	isloggedin() {

		let token = localStorage.getItem('token');
		if (token == 'login') return true;
	}

	logout(){
		this.token = undefined;
		localStorage.removeItem('token')

	}
}