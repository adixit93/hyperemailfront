import {Injectable} from "angular2/core";
import {Todo} from '../interfaces/Todos';

@Injectable()
export class TodoService {
	public todos : Array<Todo>;

	constructor() {
		this.todos = new Array<Todo>();
	}

	addToList(item : Todo){
		// console.log(item);
		this.todos = [...this.todos, item];
		console.log(this.todos);
		// this.todos.push(item);
	}

	changeState(){
		console.log(this.todos);
		this.todos = [...this.todos];
	}

	deleteTodo(idx){
		this.todos.splice(idx, 1);
		this.todos = [...this.todos];
	}

	
}