System.register(['angular2/testing', '../app.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var testing_1, app_component_1;
    return {
        setters:[
            function (testing_1_1) {
                testing_1 = testing_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            }],
        execute: function() {
            testing_1.describe('AppComponent', function () {
                testing_1.beforeEach(function () {
                    this.app = new app_component_1.AppComponent();
                });
                testing_1.it('should have name property', function () {
                    console.log("testing...");
                    testing_1.expect(this.app.title).toBe('Main App');
                });
                // it('should say hello with name property', function() {
                // 	expect(this.app.sayHello()).toBe('Hello John');
                // });
            });
        }
    }
});
//# sourceMappingURL=app.component.spec.js.map