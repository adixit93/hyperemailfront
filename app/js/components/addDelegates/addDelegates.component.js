System.register(['angular2/core', 'angular2/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1;
    var AddDelegatesComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            AddDelegatesComponent = (function () {
                function AddDelegatesComponent(router) {
                    this.router = router;
                    this.title = "Email Delegation : Who may help me?";
                }
                ;
                AddDelegatesComponent.prototype.ngOnInit = function () {
                    $('.collapsible').collapsible({
                        accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                    });
                    $('.dropdown-button').dropdown({
                        inDuration: 300,
                        outDuration: 225,
                        constrain_width: true,
                        hover: false,
                        gutter: 0,
                        belowOrigin: true,
                        alignment: 'left' // Displays dropdown with edge aligned to the left of button
                    });
                    Materialize.updateTextFields();
                };
                AddDelegatesComponent = __decorate([
                    core_1.Component({
                        selector: 'hm-add-delegates',
                        templateUrl: 'app/ts/components/addDelegates/addDelegates.component.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: []
                    }), 
                    __metadata('design:paramtypes', [router_1.Router])
                ], AddDelegatesComponent);
                return AddDelegatesComponent;
            }());
            exports_1("AddDelegatesComponent", AddDelegatesComponent);
        }
    }
});
//# sourceMappingURL=addDelegates.component.js.map