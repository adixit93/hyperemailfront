System.register(['angular2/core', '../../services/todo.service', '../../interfaces/Todos', '../../pipes/done.pipe', '../../pipes/notdone.pipe'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, todo_service_1, Todos_1, done_pipe_1, notdone_pipe_1;
    var ChecklistComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (todo_service_1_1) {
                todo_service_1 = todo_service_1_1;
            },
            function (Todos_1_1) {
                Todos_1 = Todos_1_1;
            },
            function (done_pipe_1_1) {
                done_pipe_1 = done_pipe_1_1;
            },
            function (notdone_pipe_1_1) {
                notdone_pipe_1 = notdone_pipe_1_1;
            }],
        execute: function() {
            ChecklistComponent = (function () {
                function ChecklistComponent(todoService) {
                    this.todoService = todoService;
                }
                ChecklistComponent.prototype.addToList = function () {
                    var todo = new Todos_1.Todo(this.item, this.status, "checkbox" + this.todoService.todos.length);
                    // console.log(todo);
                    this.todoService.addToList(todo);
                    console.log(this.todoService.todos);
                    this.item = "";
                    this.status = false;
                };
                ChecklistComponent.prototype.delete = function (idx) {
                    this.todoService.deleteTodo(idx);
                };
                ChecklistComponent.prototype.changeState = function (idx) {
                    this.todoService.changeState();
                    // console.log(this.todoService.todos);
                };
                ChecklistComponent = __decorate([
                    core_1.Component({
                        selector: 'check-list',
                        templateUrl: 'app/ts/components/checklist/checklist.component.html',
                        pipes: [done_pipe_1.DonePipe, notdone_pipe_1.NotDonePipe],
                        providers: [todo_service_1.TodoService]
                    }), 
                    __metadata('design:paramtypes', [todo_service_1.TodoService])
                ], ChecklistComponent);
                return ChecklistComponent;
            }());
            exports_1("ChecklistComponent", ChecklistComponent);
        }
    }
});
//# sourceMappingURL=checklist.component.js.map