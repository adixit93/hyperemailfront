System.register(['angular2/core', 'angular2/router', './components/loginForm/loginform.component', './components/signup/signup.component', './components/dashboard/dashboard.component', './components/myaccounts/myaccounts.component', './components/emailDelegation/emailDelegation.component', './components/delegation/delegation.component', './components/addDelegates/addDelegates.component', './components/folderPermission/folderPermission.component', './components/privateMessages/privateMessages.component', './components/privateContacts/privateContacts.component', './components/assignPermission/assignPermission.component', './shared/navbar/navbar.component', './shared/footer/footer.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, loginform_component_1, signup_component_1, dashboard_component_1, myaccounts_component_1, emailDelegation_component_1, delegation_component_1, addDelegates_component_1, folderPermission_component_1, privateMessages_component_1, privateContacts_component_1, assignPermission_component_1, navbar_component_1, footer_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (loginform_component_1_1) {
                loginform_component_1 = loginform_component_1_1;
            },
            function (signup_component_1_1) {
                signup_component_1 = signup_component_1_1;
            },
            function (dashboard_component_1_1) {
                dashboard_component_1 = dashboard_component_1_1;
            },
            function (myaccounts_component_1_1) {
                myaccounts_component_1 = myaccounts_component_1_1;
            },
            function (emailDelegation_component_1_1) {
                emailDelegation_component_1 = emailDelegation_component_1_1;
            },
            function (delegation_component_1_1) {
                delegation_component_1 = delegation_component_1_1;
            },
            function (addDelegates_component_1_1) {
                addDelegates_component_1 = addDelegates_component_1_1;
            },
            function (folderPermission_component_1_1) {
                folderPermission_component_1 = folderPermission_component_1_1;
            },
            function (privateMessages_component_1_1) {
                privateMessages_component_1 = privateMessages_component_1_1;
            },
            function (privateContacts_component_1_1) {
                privateContacts_component_1 = privateContacts_component_1_1;
            },
            function (assignPermission_component_1_1) {
                assignPermission_component_1 = assignPermission_component_1_1;
            },
            function (navbar_component_1_1) {
                navbar_component_1 = navbar_component_1_1;
            },
            function (footer_component_1_1) {
                footer_component_1 = footer_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                }
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'hyper-email',
                        directives: [router_1.ROUTER_DIRECTIVES, navbar_component_1.NavbarComponent, footer_component_1.FooterComponent],
                        templateUrl: 'app/ts/app.component.html'
                    }),
                    router_1.RouteConfig([
                        { path: '/', redirectTo: ['Login'] },
                        { path: '/login', name: 'Login', component: loginform_component_1.LoginComponent },
                        { path: '/signup', name: 'Signup', component: signup_component_1.SignupComponent },
                        { path: '/dashboard', name: 'Dashboard', component: dashboard_component_1.DashboardComponent },
                        { path: '/accounts', name: 'Accounts', component: myaccounts_component_1.MyAccountsComponent },
                        { path: '/delegation', name: 'Delegation', component: emailDelegation_component_1.EmailDelegationComponent },
                        { path: '/add/delegates', name: 'AddDelegates', component: addDelegates_component_1.AddDelegatesComponent },
                        { path: '/folderAccess', name: 'FolderAccess', component: folderPermission_component_1.FolderPermissionComponent },
                        { path: '/private/contacts', name: 'PrivateContacts', component: privateContacts_component_1.PrivateContactsComponent },
                        { path: '/private/messages', name: 'PrivateMessages', component: privateMessages_component_1.PrivateMessagesComponent },
                        { path: '/permissions', name: 'Permissions', component: assignPermission_component_1.AssignPermissionComponent },
                        { path: '/dashboard/emaildelegations', name: 'EmailDelegation', component: delegation_component_1.DelegationComponent },
                    ]), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map